Release notes
=============

.. toctree::
   :maxdepth: 1

   9.12.1-notes
   9.12.2-notes
